﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Threading;
using System.Printing;

namespace PrinterStatusAlert
{
    class Program
    {
        // -- Error Codes List -- //
        static Dictionary<int, string> detectedErrorState = new Dictionary<int, string>
        {
            { 0, "Unknown" }, { 1, "Other" }, { 2, "No Error" }, { 3, "Low Paper" },
            { 4, "No Paper" }, { 5, "Low Toner" }, { 6, "No Toner" }, { 7, "Door Open" },
            { 8, "Jammed" }, { 9, "Offline" }, { 10, "Service Requested" }, { 11, "Output Bin Full" }
        };

        static Dictionary<int, string> extendedDetectedErrorState = new Dictionary<int, string>
        {
            { 0, "Unknown" }, { 1, "Other" }, { 2, "No Error" }, { 3, "Low Paper" },
            { 4, "No Paper" }, { 5, "Low Toner" }, { 6, "No Toner" }, { 7, "Door Open" },
            { 8, "Paper Jam" }, { 9, "Service Requested" }, { 10, "Output Bin Full" }, { 11, "Paper problem" },
            { 12, "Cannot Print Page" }, { 13, "User Intervention Required" }, { 14, "Out of Memory" }, { 15, "Server Unknown" }
        };

        static Dictionary<int, string> extendedPrinterStatus = new Dictionary<int, string>
        {
            { 1, "Other" }, { 2, "Unknown" }, { 3, "Idle" }, { 4, "Printing" },
            { 5, "Warming Up" }, { 6, "Stopped Printing" }, { 7, "Offline" },
            { 8, "Paused" }, { 9, "Error" }, { 10, "Busy" }, { 11, "Not Available" },
            { 12, "Waiting" }, { 13, "Processing" }, { 14, "Initialization" }, { 15, "Power Save" },
            { 16, "Pending Deletion" }, { 17, "I/O Active" }, { 18, "Manual Feed" }
        };

        static Dictionary<string, string> status = new Dictionary<string, string>
        {
            { "OK","OK" }, { "Error", "Error" }, { "Degraded", "Degraded" }, {"Unknown", "Unknown" },
            {"Pred Fail", "Pred Fail" }, {"Starting", "Starting" }, { "Stopping", "Stopping"},
            {"Service", "Service" }, {"Stressed", "Stressed" }, {" NonRecover", "NonRecover" },
            {"No Contact", "No Contact" }, {" Lost Comm", "Lost Comm" }
        };

        static Dictionary<int, string> printerStatus = new Dictionary<int, string>
        {
            { 1, "Other" }, { 2, "Unknown" }, { 3, "Idle" },
            { 4, "Printing" }, { 5, "Warming Up" }, { 6, "Stopped Printing" }, { 7, "Offline" }
        };

        //Depreciated by Microsoft -- Don't Use -- 
        //static Dictionary<int, string> printerState = new Dictionary<int, string>
        //{
        //    { 0, "Unknown" }, { 1, "Other" }, { 2, "No Error" }, { 3, "Low Paper" },
        //    { 4, "No Paper" }, { 5, "Low Toner" }, { 6, "No Toner" }, { 7, "Door Open" },
        //    { 8, "Jammed" }, { 9, "Offline" }, { 10, "Service Requested" }, { 11, "Output Bin Full" }
        //};

        static void Main(string[] args)
        {

            List<string> printerList = new List<string>(new string[] { "RN13", "PG29", "PD34", "RA66", "RE26", "RA66", "RA69"});
            //List<string> printerList = new List<string>(new string[] {"PD34" });
            //List<string> printerListHome = new List<string>(new string[] { "Adobe PDF" });

            //Console.WriteLine(item.ToString());
            consoleWritePrinterError(printerList);
            getJobListInfo("PD34");

        }

        // -------------------------------------------------------------------------------------------------
        // -- Printer JobStatus -- own method?
        //      -- Enumerate job list for selected printer
        //      -- run IsInError(), isPaused(), isBlocked(), isPaperOut() etc..
        //https://msdn.microsoft.com/en-us/library/system.printing.printsystemjobinfo.aspx


        //Console.WriteLine(info);


        static List<KeyValuePair<string, object>> getPrinterStatus(string p)
        {
            string printerName = p;
            string query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName);
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection coll = searcher.Get();

            var printerStatusCodes = new List<KeyValuePair<string, object>>();

            foreach (ManagementObject printer in coll)
            {
                foreach (PropertyData property in printer.Properties)
                {
                    if (property.Name.Equals("Status") ||
                        property.Name.Equals("DetectedErrorState") ||
                        property.Name.Equals("ExtendedDetectedErrorState") ||
                        property.Name.Equals("PrinterStatus") ||
                        property.Name.Equals("ExtendedPrinterStatus")
                        )
                    {
                        printerStatusCodes.Add(new KeyValuePair<string, object>(property.Name, property.Value));
                        //Console.WriteLine(string.Format("{0}: {1}", property.Name, property.Value));
                    }
                    //Console.WriteLine(string.Format("{0}: {1}", property.Name, property.Value));
                }
            }
            return printerStatusCodes;

        }

        static string getErrorDef(string errorClass, int errorCode)
        {
            //string errorMessage;
            switch (errorClass)
            {
                case "DetectedErrorState":
                    return detectedErrorState[errorCode];

                case "ExtendedDetectedErrorState":
                    return extendedDetectedErrorState[errorCode];

                case "ExtendedPrinterStatus":
                    return extendedPrinterStatus[errorCode];

                case "PrinterStatus":
                    return printerStatus[errorCode];


                default:
                    break;
            }
            return null;
        }

        //Actually gets jobs and their statuses.
        static string getJobListInfo(string printerName)
        {
            PrintServer pserver = new PrintServer(@"\\HC-PRINT11");
            PrintQueue printQueue = pserver.GetPrintQueue(printerName);
            //pserver.Refresh();

            Console.WriteLine(printQueue.IsInError);  //is returning false even when printer is in error. Must be for something else.
            //Console.WriteLine("Description: " + printQueue.Description);
            //Console.WriteLine("Name: " + printQueue.Name);
            //var pServerQueue = pserver.GetPrintQueue(printQueue);
            PrintJobInfoCollection info;
            info = printQueue.GetPrintJobInfoCollection();

            foreach (var item in info)
            {
                Console.WriteLine("Job Status: " + item.JobStatus);
                Console.WriteLine("Job Name: " + item.Name);
            }
            return null;
        }

        static void consoleWritePrinterError(List<string> printerList)
        {


            foreach (string item in printerList)
            {
                Console.WriteLine(item + ":");
                Console.WriteLine("");
                
                foreach (var printerObj in getPrinterStatus(item))
                {
                    var key = printerObj.Key;
                    if (printerObj.Key != "Status")
                    {
                        int value = Convert.ToInt32(printerObj.Value);
                        Console.WriteLine(printerObj.Key + ": " + getErrorDef((printerObj.Key), value));
                        //Console.WriteLine(printerObj.Key + ": " + printerObj.Value + " -- " + printerObj.Key[0]);
                    }

                    else
                    {
                        if (printerObj.Key.Equals("Status"))
                        {
                            string statKey = printerObj.Value.ToString();
                            Console.WriteLine(printerObj.Key + ": " + status[statKey]);
                        }
                        Console.WriteLine("");
                    
                    }
                }
            }
        }
    }
}
